const stringToArray = (param: string) =>
    new Array(param.length).fill(null).map((_, index) => param[index]);

export default stringToArray;