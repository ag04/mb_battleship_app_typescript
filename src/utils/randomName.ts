const randomName = (array: string[]) => array[Math.floor(Math.random() * array.length)];

export default randomName;
