import stringToArray from './stringToArray';
import { RootState } from '../reducers';

const getSelfBoard = (state: RootState) => {
    if (!state.game.self) {
        return [[]];
    }

    return state.game.self.board.map(stringToArray);
};

export default getSelfBoard;
