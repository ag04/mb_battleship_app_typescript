import { RootState } from '../reducers';

const geWinner = (state: RootState) => {
    if (!state.game.game) {
        return null;
    }

    if (state.game.game.game.won === '') {
        return null;
    }

    return state.game.game.game.won;

};

export default geWinner;
