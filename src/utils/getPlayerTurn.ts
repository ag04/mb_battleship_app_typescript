import { RootState } from '../reducers';

const getPlayerTurn = (state: RootState) => {
    if (!state.game.game) {
        return null;
    }

    if (state.game.game.game.player_turn === '') {
        return null;
    }

    return state.game.game.game.player_turn;

};

export default getPlayerTurn;
