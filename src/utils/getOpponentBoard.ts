import stringToArray from './stringToArray';
import { RootState } from '../reducers';

const getOpponentBoard = (state: RootState) => {
    if (!state.game.opponent) {
        return [[]];
    }
    return state.game.opponent.board.map(stringToArray);
};

export default getOpponentBoard;
