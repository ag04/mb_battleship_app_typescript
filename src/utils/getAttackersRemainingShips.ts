import { RootState } from '../reducers';

const getAttackersRemainingShips = (state: RootState) => {
    if (!state.game.self) {
        return null;
    }

    return state.game.self.remaining_ships;
};

export default getAttackersRemainingShips;
