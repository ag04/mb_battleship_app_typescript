import * as React from 'react';
import { connect } from 'react-redux';
import { challengePlayer, fetchPlayers } from '../actions';
import PageHeader from './PageHeader';
import { Button } from 'semantic-ui-react';
import styled from 'styled-components';
import * as _ from 'lodash';
import { Link, RouteComponentProps } from 'react-router-dom';
import { RootState } from '../reducers';

const Container = styled.div`
    max-width: 700px;
    padding-top: 20px;
    margin: auto;
`;

class PlayerChallenger extends React.Component<ReduxState & DispatchProps & RouteComponentProps<{playerId: string}>> {
    public state = {
        player_id: '',
    };

    public componentDidMount() {
        this.props.fetchPlayers();
    }

    public handleSubmit() {
        this.props.challengePlayer(this.props.match.params.playerId, this.state, this.props.history.push);
    };

    public onClickHandle = (id: string) => () => {
        this.setState({player_id: id})
    };

    public renderPlayers() {
        return (
            _.map(this.props.players, (player: {id: string, name: string, email: string}) => {
                if (player.id === this.props.match.params.playerId) {
                    return null;
                } else {
                    return (
                        <div className="field" key={player.id}>
                            <div className="ui radio checkbox">
                                <input
                                    type="radio"
                                    name="player"
                                    checked={player.id === this.state.player_id}
                                    className="hidden"
                                />
                                <label
                                    onClick={this.onClickHandle(player.id)}>{_.capitalize(player.name)}</label>
                            </div>
                        </div>
                    )
                }
            })
        );
    }

    public render() {
        return (
            <Container>
                <PageHeader/>
                <div className="ui form" style={{fontSize: 'large', marginTop: '50px'}}>
                    <div className="grouped fields">
                        <label htmlFor="player">Who is the challenger?</label>
                        {this.renderPlayers()}
                    </div>
                    <Button basic color="blue" disabled={!this.state.player_id} onClick={this.handleSubmit.bind(this)}>Start
                        game!</Button>
                    <Link to={'/player/list'}>
                        <Button basic>Cancel</Button>
                    </Link>
                </div>
            </Container>
        );
    };
}

function mapStateToProps(state: RootState) {
    return {
        players: state.players,
    };
}

type ReduxState = ReturnType<typeof mapStateToProps>;

const mapDispatchToProps = {
    fetchPlayers,
    challengePlayer,
};

type DispatchProps = typeof mapDispatchToProps

export default connect(mapStateToProps, mapDispatchToProps)(PlayerChallenger);
