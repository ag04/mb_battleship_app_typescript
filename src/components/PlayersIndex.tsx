import * as React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { RootState } from '../reducers';
import { fetchPlayerGameList, fetchPlayers } from '../actions';
import randomName from '../utils/randomName';
import * as _ from 'lodash';
import { Accordion, Button, Menu, Image } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import PageHeader from './PageHeader';
import PlayerContent from './PlayerContent';

const Container = styled.div`
    max-width: 1000px;
    padding-top: 20px;
    margin: auto;
`;

interface IState {
    activeIndex: number,
    avatarMap: {},
}

interface PlayerValue {
    name: string,
    email: string,
    id: string,
    games: GameValue[],
}

interface GameValue {
    opponent_id: string,
    game_id: string,
    status: string,
}

class PlayersIndex extends React.Component<ReduxState & DispatchProps, IState> {
    public state = {
        activeIndex: 0,
        avatarMap: {},
    };

    public componentDidUpdate() {
        if (this.state.avatarMap === undefined && this.props.players) {
            const avatarMap = {};
            Object.keys(this.props.players).forEach(key => avatarMap[key] = randomName(['rachel', 'lindsay', 'matthew', 'mark', 'molly', 'lena']));
            this.setState({avatarMap});
        }
    }

    public componentDidMount() {
        this.props.fetchPlayers();
    }

    public renderPlayers() {
        const {activeIndex} = this.state;

        if (this.state.avatarMap === undefined) {
            return null;
        }

        return _.map(this.props.players, (player: PlayerValue) => {
            const PlayerTitle = <React.Fragment>
                <Image
                    avatar={true}
                    src={`https://react.semantic-ui.com/images/avatar/small/${this.state.avatarMap[player.id]}.png`}
                    style={{marginRight: '10px'}}/>
                <b>{_.capitalize(player.name)}</b>
            </React.Fragment>;

            return (
                <Menu.Item key={player.id}>
                    <Accordion.Title
                        active={activeIndex.toString() === player.id}
                        index={player.id}
                        onClick={this.handleClick}
                        content={PlayerTitle}
                    />
                    <PlayerContent player={player} players={this.props.players} active={activeIndex.toString()}/>
                </Menu.Item>
            )
        })
    }

    public render() {
        return (
            <Container>
                <PageHeader/>
                {!Object.keys(this.props.players).length ? <p><b>No players to show. Please, add a new one.</b></p> :
                    <Accordion as={Menu} vertical={true} fluid={true}>
                        {this.renderPlayers()}
                    </Accordion>
                }
                <Link to={'/player'}>
                    <Button basic={true} color="blue">Add player</Button>
                </Link>
            </Container>
        );
    }

    private handleClick = (_: any, titleProps: { index: number }) => {
        const {index} = titleProps;
        const {activeIndex} = this.state;
        const newIndex = activeIndex === index ? -1 : index;

        if (newIndex !== -1) {
            this.props.fetchPlayerGameList(String(newIndex));
        }
        this.setState({activeIndex: newIndex});
    };
}

function mapStateToProps(state: RootState) {
    return {
        players: state.players,
    };
}

type ReduxState = ReturnType<typeof mapStateToProps>;

const mapDispatchToProps = {
    fetchPlayers,
    fetchPlayerGameList,
};

type DispatchProps = typeof mapDispatchToProps

export default connect(mapStateToProps, mapDispatchToProps)(PlayersIndex);
