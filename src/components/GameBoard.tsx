import * as React from 'react';
import styled from 'styled-components';

const TableContainer = styled.div`
  margin: 10px;
`;

const Cell = styled.td`
  width: 45px;
  height: 45px;
`;

const CellSelected = styled(Cell)`
  background-color: #35e735;
`;

const CellShip = styled(Cell)`
  background-color: deepskyblue;
`;

const CellMiss = styled(Cell)`
  background-color: #e7e7e7;
`;

const CellHit = styled(Cell)`
  background-color: orangered;
`;

type OnClick = (a: string, b: string, add: any) => void;

interface Props {
    board: string[][],
    onClick?: OnClick,
    salvo?: string[][],
}

const magicSuperExtraMagicFunction = (fnc: OnClick, a: string, b: string, add: any) => () => {
    fnc(a, b, add);
};

const GameBoard = (props: Props) =>
    <TableContainer>
        <table className="ui compact celled table">
            <tbody>
            {props.board.map((row, index) => <tr key={index}>
                {row.map((cell, rowIndex) => {
                    switch (cell) {
                        case '#':
                            return <CellShip key={rowIndex}/>;
                        case 'O':
                            return <CellMiss key={rowIndex}/>;
                        case 'X':
                            return <CellHit key={rowIndex}/>;
                        default:
                            if (props.onClick) {
                                let boardCell =
                                    <Cell key={rowIndex}
                                       onClick={magicSuperExtraMagicFunction(
                                           props.onClick,
                                           index.toString(),
                                           rowIndex.toString(),
                                           true)}
                                    />;
                                if (props.salvo) {
                                    props.salvo.forEach(shot => {
                                        if (shot[0] === index.toString() && shot[1] === rowIndex.toString()) {
                                            if (props.onClick) {
                                                boardCell = <CellSelected key={rowIndex}
                                                                          onClick={magicSuperExtraMagicFunction(
                                                                              props.onClick,
                                                                              index.toString(),
                                                                              rowIndex.toString(),
                                                                              false)}/>
                                            } else {
                                                boardCell = <CellSelected key={rowIndex}/>
                                            }
                                        }
                                    })
                                }
                                return boardCell;
                            }
                            return <Cell key={rowIndex}/>;
                    }
                })}
            </tr>)}
            </tbody>
        </table>
    </TableContainer>
;

export default GameBoard;
