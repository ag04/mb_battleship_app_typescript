import * as React from 'react';
import { Route, Switch } from 'react-router';
import PlayersIndex from './components/PlayersIndex';
import PlayerNew from './components/PlayerNew';
import GameIndex from './components/GameIndex';
import ActiveGame from './components/ActiveGame';
import PlayerChallenger from './components/PlayerChallenger';

const App: React.SFC<{}> = () =>
    <Switch>
        <Route path={'/game/:gameId/turn/:playerTurn'} component={ActiveGame}/>
        <Route path={'/game/challenge/:playerId'} component={PlayerChallenger}/>
        <Route path={'/player/:playerId/game/:gameId'} component={GameIndex}/>
        <Route path={'/player/list'} component={PlayersIndex}/>
        <Route path={'/player'} component={PlayerNew}/>
        <Route path={'/'} component={PlayersIndex}/>
    </Switch>;

export default App;
