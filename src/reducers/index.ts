import { combineReducers } from 'redux';
import { StateType } from 'typesafe-actions';
import reducerGame from './reducerGame';
import reducerPlayers from './reducerPlayers';

const rootReducer = combineReducers({
    players: reducerPlayers,
    game: reducerGame,
});

export default rootReducer;

export type RootState = StateType<typeof rootReducer>;
