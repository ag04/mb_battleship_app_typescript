import {
    AUTOPILOT,
    AUTOPILOT_OFF,
    CHALLENGE_PLAYER,
    FETCH_GAME,
    FINISHED_SHOT,
    FIRING_SHOT,
    SHOT_SALVO,
} from '../actions';
import produce from 'immer';
import stringToArray from '../utils/stringToArray';

const initialState = {
    firing: false,
    self: {
        player_id: '',
        board: [],
        autopilot: false,
        remaining_ships: 10,
    },
    opponent: {
        player_id: '',
        board: [],
        autopilot: false,
        remaining_ships: 10,
    },
    game: {
        self: {
            player_id: '',
            board: [],
            autopilot: false,
            remaining_ships: 10,
        },
        opponent: {
            player_id: '',
            board: [],
            autopilot: false,
            remaining_ships: 10,
        },
        game: {
            player_turn: '',
            won: '',
        },
    },
};

interface StateValues {
    firing: boolean,
    self: {
        player_id: string,
        board: string[],
        autopilot: boolean,
        remaining_ships: number,
    },
    opponent: {
        player_id: string,
        board: string[],
        autopilot: boolean,
        remaining_ships: number,
    },
    game: {
        self: {
            player_id: string,
            board: string[],
            autopilot: boolean,
            remaining_ships: number,
        },
        opponent: {
            player_id: string,
            board: string[],
            autopilot: boolean,
            remaining_ships: number,
        },
        game: {
            player_turn: string,
            won: string,
        },
    },
}

const reducerGame = produce<StateValues, any>(
    (state, action) => {
        switch (action.type) {
            case FETCH_GAME:
                return action.payload;

            case FIRING_SHOT:
                state.firing = true;
                return;

            case FINISHED_SHOT:
                state.firing = false;
                return;

            case SHOT_SALVO:
                const {salvo} = action.payload.data;
                const keys = Object.keys(salvo);
                keys.forEach(key => {
                    const cell = key.split('x');
                    const column = cell[1].charCodeAt(0) - 65;

                    const row = state.opponent.board[cell[0]];
                    const rowArray = stringToArray(row);

                    switch (salvo[key]) {
                        case 'MISS':
                            rowArray[column] = 'O';
                            break;
                        case 'HIT':
                            rowArray[column] = 'X';
                            break;
                        case 'KILL':
                            rowArray[column] = 'X';
                            break;
                    }
                    state.opponent.board[cell[0]] = rowArray.join('');
                });
                return;

            case CHALLENGE_PLAYER:
                return action.payload.data;

            case AUTOPILOT:
                if (action.meta.playerId === state.self.player_id) {
                    state.self.autopilot = true;
                } else {
                    state.opponent.autopilot = true;
                }
                return;

            case AUTOPILOT_OFF:
                if (action.meta.playerId === state.self.player_id) {
                    state.self.autopilot = false;
                } else {
                    state.opponent.autopilot = false;
                }
                return;
        }
    }, initialState,
);

export default reducerGame;
