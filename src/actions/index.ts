import axios from 'axios';
import { Dispatch } from 'redux';

export const FETCH_PLAYERS = 'fetch_players';
export const CREATE_PLAYER = 'create_player';
export const FETCH_PLAYER_GAME_LIST = 'fetch_playerGameList';
export const FETCH_GAME = 'fetch_game';
export const FETCH_PLAYER = 'fetch_player';
export const SHOT_SALVO = 'shot_salvo';
export const CHALLENGE_PLAYER = 'challenge_player';
export const AUTOPILOT = 'autopilot';
export const AUTOPILOT_OFF = 'autopilot_off';
export const FIRING_SHOT = 'firing_shot';
export const FINISHED_SHOT = 'finished_shot';

const ROOT_URL = process.env.REACT_APP_API_URL;

interface CreatePlayerValues {
    id: string,
    name: string,
    email: string,
}

interface CreateSalvoValues {
    salvo: string[],
}

interface CreateChallengePlayerValues {
    player_id: string,
}

interface CreateUpdateGameValues {
    self: {
        player_id: string,
        board: string[],
        autopilot: boolean,
        remaining_ships: number,
    },
    opponent: {
        player_id: string,
        board: string[],
        autopilot: boolean,
        remaining_ships: number,
    },
    game: {
        player_turn?: string,
        won?: string,
    }
}

type Callback = () => void;

export function fetchPlayers() {
    return (dispatch: Dispatch) => {
        axios.get(`${ROOT_URL}/player/list`)
            .then(result => {
                dispatch({
                    payload: result,
                    type: FETCH_PLAYERS,
                });
            });
    };
}

export function createPlayer(values: CreatePlayerValues, callback: Callback) {
    return (dispatch: Dispatch) => {
        axios.post(`${ROOT_URL}/player`, values)
            .then(result => {
                dispatch({
                    type: CREATE_PLAYER,
                    payload: result,
                });
                callback();
            });
    };
}

export function fetchPlayerGameList(playerId: string) {
    return (dispatch: Dispatch) => {
        axios.get(`${ROOT_URL}/player/${playerId}/game/list`)
            .then(result => {
                dispatch({
                    type: FETCH_PLAYER_GAME_LIST,
                    payload: result,
                    meta: playerId,
                });
            });
    }
}

export function fetchGame(playerId: string, gameId: string) {
    return (dispatch: Dispatch) => {
        axios.get(`${ROOT_URL}/player/${playerId}/game/${gameId}`)
            .then(result => {
                dispatch({
                    type: FETCH_GAME,
                    payload: result.data,
                    meta: {playerId, gameId},
                });
            });
    }
}

export function fetchPlayer(playerId: string) {
    return (dispatch: Dispatch) => {
        axios.get(`${ROOT_URL}/player/${playerId}`)
            .then(result => {
                dispatch({
                    type: FETCH_PLAYER,
                    payload: result,
                    meta: playerId,
                });
            });
    }
}

export function shotSalvo(
    playerId: string,
    opponentId: string,
    gameId: string,
    values: CreateSalvoValues,
    callback: (playerId: string, gameId: string) => void,
) {
    return (dispatch: Dispatch) => {
        dispatch({
            type: FIRING_SHOT,
        });
        axios.put(`${ROOT_URL}/player/${playerId}/game/${gameId}`, values)
            .then(result => {
                dispatch({
                    type: SHOT_SALVO,
                    payload: result,
                });
                dispatch({
                    type: FINISHED_SHOT,
                });
            })
            .catch(() => {
                    callback(playerId, gameId);
                },
            );
    };
}

export function challengePlayer(opponentId: string, values: CreateChallengePlayerValues, historyPush: (url: string) => void) {
    return (dispatch: Dispatch) => {
        axios.post(`${ROOT_URL}/player/${opponentId}/game`, values)
            .then(result => {
                dispatch({
                    type: CHALLENGE_PLAYER,
                    payload: result,
                });
                historyPush(`/game/${result.data.game_id}/turn/${result.data.starting}`);
            });
    }
}

export function turnOnAutopilot(playerId: string, gameId: string) {
    return (dispatch: Dispatch) => {
        axios.put(`${ROOT_URL}/player/${playerId}/game/${gameId}/autopilot`)
            .then( () => {
                dispatch({
                    type: AUTOPILOT,
                    meta: {playerId, gameId},
                });
            });
    }
}

export function turnOffAutopilot(playerId: string, gameId: string) {
    return (dispatch: Dispatch) => {
        axios.put(`${ROOT_URL}/player/${playerId}/game/${gameId}/autopilot/off`)
            .then( () => {
                dispatch({
                    type: AUTOPILOT_OFF,
                    meta: {playerId, gameId},
                });
            });
    }
}

export function updateGame(gameStatus: CreateUpdateGameValues, gameId: string) {
    return (dispatch: Dispatch) => {
        dispatch({
            type: FETCH_GAME,
            payload: gameStatus,
            meta: {playerId: gameStatus.self.player_id, gameId},
        });
    }
}
